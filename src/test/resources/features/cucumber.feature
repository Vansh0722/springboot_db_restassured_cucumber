Feature:Verify different GET operations using REST-assured

  Scenario: Verify one author of the post
    Given I perform GET operation for "</api/employee/get-all-employees>"
    And I perform GET for the employee number "22"
    Then I should see the employee name as "Harsh"



  Scenario: Add a new employee

    Given The valid endpoint with payload to create employee
    When The request is send to the server
    Then The new employee must be created with name as “<username>”


  Scenario: Verify collections of employees
    Given I perform GET operation for "</api/employee/get-all-employees>"
    Then I should see the employee names


  Scenario: PUT details of a  employee

    Given I ensure to perform PUT operation
    When I perform PUT for the employee number "3" and should be updated
    Then employee id and should be updated


  Scenario: DELETE details of a  employee
    Given when i ensure  to perform DELETE operation
    When I perform DELETE for the employee number "<employeeId>"