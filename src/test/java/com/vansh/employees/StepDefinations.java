package com.vansh.employees;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import lombok.extern.log4j.Log4j2;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.testng.Assert;

import java.sql.*;
import java.util.HashMap;

import static io.restassured.RestAssured.*;
import static java.util.EnumSet.allOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
//import static sun.nio.cs.Surrogate.is;

@Log4j2
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class StepDefinations {
    Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres","vansh","root");
    Statement statement=null;
    ResultSet result = null;

    ResultSet resultOfAllEmployees = null;

    public StepDefinations() throws SQLException {
    }



    @Given("I perform GET operation for {string}")
    public void i_perform_get_operation_for(String url) {

        given().contentType(ContentType.JSON);
    }

    @And("I perform GET for the employee number {string}")
    public void i_perform_get_for_the_employee_number(String employeeNumber) {
        when().get(String.format("http://localhost:8080/api/employee/get-employee/%s", employeeNumber))
                .then().body("name", containsString("Harsh")).
                statusCode(200).log().all();
    }

    @Then("I should see the employee name as {string}")
    public void i_should_see_the_employee_name_as(String string) throws SQLException {
        statement = connection.createStatement();
        result= statement.executeQuery("SELECT * FROM public.employees WHERE id=22");

        result.next();
        Assert.assertEquals(result.getString("name"),"Harsh");
        Assert.assertEquals(result.getString("location"),"India");
    }


    //    Add a new employee
    Response response;

    public HashMap<Object, Object> newEmployee = new HashMap<Object, Object>();

    @Given("The valid endpoint with payload to create employee")
    public void the_valid_endpoint_with_payload_to_create_employee() {
        RestAssured.baseURI = "http://localhost:8080";
        RestAssured.basePath = "/api/employee/create-employees";

        newEmployee.put("name", "John");
        newEmployee.put("location", "England");
        newEmployee.put("emailId", "john@gmail.com");
    }

    @When("The request is send to the server")
    public void the_request_is_send_to_the_server() {
        response = given()
                .contentType(ContentType.JSON)
                .body(newEmployee)
                .when()
                .post()
                .then()
                .body("name", containsString("John"))
                .log().all()
                .statusCode(200).contentType(ContentType.JSON).
                extract().response();
        System.out.println(response);
    }

    @Then("The new employee must be created with name as “<username>”")
    public void the_new_employee_must_be_created_with_name_as_username() throws SQLException {
        statement = connection.createStatement();
        result= statement.executeQuery("SELECT * FROM public.employees WHERE id=165");
        result.next();
        Assert.assertEquals(result.getString("name"),"John");
        Assert.assertEquals(result.getString("location"),"England");

    }


    //Get all the employees
    @Then("I should see the employee names")
    public void i_should_see_the_employee_names() throws SQLException {
        when().get(String.format("http://localhost:8080/api/employee/get-all-employees"));
        statement = connection.createStatement();
        result= statement.executeQuery("SELECT count(*) FROM employees");
        result.next();
        Assert.assertEquals(result.getString("count"), "3");


    }



    //PUT/PATCh operation
    @Given("I ensure to perform PUT operation")
    public void i_ensure_to_perform_put_operation() {

        RestAssured.baseURI = "http://localhost:8080";
        RestAssured.basePath = "/api/employee/165";

        newEmployee.put("name", "John Deo2");
        newEmployee.put("location", "England");
        newEmployee.put("emailId", "johndeo@gmail.com");
    }

    @When("I perform PUT for the employee number {string} and should be updated")
    public void i_perform_put_for_the_employee_number_and_should_be_updated(String string) throws SQLException {
        response = given()
                .contentType(ContentType.JSON)
                .body(newEmployee)
                .when()
                .put()
                .then().
                assertThat()
                .body("name", containsString("John Deo2"))
                .log().all()
                .statusCode(200).contentType(ContentType.JSON).
                extract().response();
    }

    @Then("employee id and should be updated")
    public void employee_id_and_should_be_updated() throws SQLException {
        statement = connection.createStatement();
        result= statement.executeQuery("SELECT * FROM public.employees WHERE id=165");
        result.next();
        Assert.assertEquals(result.getString("name"),"John Deo2");

    }


    //DELETE method
    @Given("when i ensure  to perform DELETE operation")
    public void when_i_ensure_to_perform_delete_operation() {
        RestAssured.baseURI = "http://localhost:8080";
        RestAssured.basePath = "/api/employee/delete-employees/165";
    }
    @When("I perform DELETE for the employee number {string}")
    public void i_perform_delete_for_the_employee_number(String string) throws SQLException {
        Response response = given()
                .contentType(ContentType.JSON)
                .when()
                .delete()
                .then()
                .log().all()
                .statusCode(200).contentType(ContentType.JSON)
                .extract().response();
        statement = connection.createStatement();
        result= statement.executeQuery("SELECT * FROM public.employees WHERE name like 'John Deo2'");
        result.next();
        Assert.assertFalse(result.rowDeleted());

    }


}