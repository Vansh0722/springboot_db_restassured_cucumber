package com.vansh.employees;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.spring.CucumberContextConfiguration;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(Cucumber.class)
@CucumberContextConfiguration

//@SpringBootTest(classes = Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features"
)

public class CucumberRunnerTest {


}
